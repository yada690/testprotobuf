package main

import (
	"demo"
	"fmt"
	"os"

	"github.com/golang/protobuf/proto"
)

func main() {
	var a int32
	a = 1

	//注意每条信息后面的,号
	msgtest := &demo.Msg{
		MsgType: proto.Int32(a),
		MsgInfo: proto.String("I am hahaya."),
		MsgFrom: proto.String("127.0.0.1"),
	}
	//将数据序列化到字符串中(写操作)
	indata, err := proto.Marshal(msgtest)
	if err != nil {
		fmt.Println("Marshaling error: ", err)
		os.Exit(1)
	}
	//将数据从字符串中反序列化出来(读操作)
	msgencoding := &demo.Msg{}
	err = proto.Unmarshal(indata, msgencoding)
	if err != nil {
		fmt.Println("Unmarshaling error: ", err)
		os.Exit(1)
	}
	fmt.Printf("msg type: %d\n", msgencoding.GetMsgType())
	fmt.Printf("msg info: %s\n", msgencoding.GetMsgInfo())
	fmt.Printf("msg from: %s\n", msgencoding.GetMsgFrom())
}
